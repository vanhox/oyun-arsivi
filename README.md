# Oyun Arşivi
#### ! Bunları açmadan önce UBlock Origin kullanın ! 
##### (*) işareti bıraktıklarım benim kullanıp sevdiğim sitelerdir.
### Araçlar
#### İndirme Yöneticileri
- [JDownloader2](https://jdownloader.org/jdownloader2) (Ücretsiz)
- [IDM](https://www.internetdownloadmanager.com/download.html) (Ücretli) Crack olarak indirmek için bu bağlantıya gidin https://1337x.to/torrent/4077667/Internet-Download-Manager-IDM-6-35-build-8-incl-Patch-Fake-Serial-Fixed-CrackingPatching/
#### Steam Araçları
- [Steam Content Packager](https://cs.rin.ru/forum/viewtopic.php?f=29&t=77526) Steam dosyalarınızı SCS forumunda paylaşmak için hazırlar.
- [Goldberg Emulator](https://gitlab.com/Mr_Goldberg/goldberg_emulator) Kendi Steam oyununu cracklemek için kullanabileceğin bir araç. (Daha önceden cracklenmemiş bir oyunu crackleyemezsin, bu süper gizli bir 0day script'i değil.)
- [SmartSteamEmu](https://cs.rin.ru/forum/viewtopic.php?f=29&t=62935) Bir üstteki araç ile aynı işlevi görüyor.
- [CreamAPI](https://cs.rin.ru/forum/viewtopic.php?t=70576) Sahip olduğum Steam oyununun DLC'lerinin kilidini açman için bir araç. 
- [AutoCreamAPI](https://cs.rin.ru/forum/viewtopic.php?p=2013521) Üstteki aracı otomatik hazır eden bir araç.
- [Steamless](https://www.reddit.com/r/Piracy/wiki/megathread/games#wiki_.25BA_introduction) Steam'in DRM'ini kaldırmak için yardımcı bir araç, emülatör ile birlikte kullanmak için geliştirilmiş.
#### Oyun Çalıştırıcıları
- [Playnite](https://playnite.link/) (Windows)
- [Gog Galaxy](https://www.gog.com/galaxy) (Windows/MacOS)
- [Lutris](https://lutris.net/) (Linux)
### Torrent Siteleri
- [Rargb](https://rarbg.to/index80.php)
- [1337x](https://1337x.to/) Güzel modere edilmiş bir site *
- [Rustorka](http://rustorka.com/forum/index.php)
- [Rutor](http://rutor.info/games)
- [GamesTorrents](https://www.gamestorrents.nu/)
- [(Repack) Fitgirl](https://fitgirl-repacks.site/) Repack için öneririm
- [(Repack) Xatab](https://x8.xatab-repack.com/)
- [(Repack) Dodi](https://dodi-repacks.site/)
- [(Repack) Kaoskrew](https://kaoskrew.org/)
- [(Repack) RG Mechanics](https://s1.rg-mechanics.org/)
### DDL Siteleri
- [cs.sin.ru](https://cs.rin.ru/forum/viewforum.php?f=10) En geniş forum *
- [Torrminatorr](https://forum.torrminatorr.com/) Linux için en iyi forum *
- [MyAbondonWare](https://www.myabandonware.com/) Retro oyun Arşivi *
- [Gload](https://gload.cc/)
- [Release BB](https://rlsbb.ru/category/games/pc/?__cf_chl_jschl_tk__=fc7db1b8b60b0de5ad3bfbc898276e081aebf32a-1612299832-0-AR2UWFWXrhXIMopzQR1_wZIbf9GXwcgwY0iSui94pj4oxRFqCQjrBYOGoPi50haNczK7-pg0vZXzBrHtRmVS0mO-QKZYVS643MQim2xGKnGnBwvBpSzvI6xRyUyeG-__CwyqonSx-Hor-Oy7XnlRJLWMJ4vIBG8nbit6qspLtrxfi3wjT2UmcPoyysSznA4XUAf9xuKBNmhNFVnIQP2ptKx6FXlMXni7RabDBqSgWxzGrmM0fuUjoGcBmovwfqFmww-wQFRxFQP_JwSOMIPuzT524Wxh-0bgMGBZGlM2wBkPUJDzt4DcTKm6WLL47Tp7YwlHp2POEuG8U-tF81pKxu0)
- [GOG Games](https://gog-games.com/) Temiz GOG oyunları **
- [Steam Unlocked](https://steamunlocked.net/) Temiz Steam oyunları **

### !!! Bunları Kullanmayın !!!
- **The Pirate Bay** (TPB): Pirate Bay'de yüksek derecede malware riski vardır, burayı kesinlikle kullanmayın.
- **Kickass Torrents** (KAT): Bu site 2016 yılında kapatıldı, sonradan açılan mirror sitelere güvenmeyin.
- **BB Repacks** (BlackBox Repacks): Bu sitede paylaşılan oyunlarda malware riski bulunmakta, daha önce Doom Eternal Repack'leri üzerinde malware tespit edildi. Kaynak: https://old.reddit.com/r/Piracy/comments/fmgyf6/doom_eternal_repack_contains_malware/
- **Qoob/Seyter** : Qoob, Seyter'in JC3 Repack dosyasında bitcoin miner tespit edilmesinden sonra değiştirdiği yeni adıdır. Kaynak: https://old.reddit.com/r/CrackStatus/comments/4x0nt1/jc3_xl_seyter_has_bitcoin_miner/ / https://old.reddit.com/r/CrackWatch/comments/a78tvj/qoob_stopped_repacking/ec18n6f/
- **CorePack** : Paylaşımlarının pek çoğunda malware tespit edildi. Kaynak: https://old.reddit.com/r/CrackWatch/comments/8wp2fa/there_are_malwares_inside_fear_3_corepack_releases/ Resmi Açıklamaları: https://old.reddit.com/r/CrackWatch/comments/8wuyyk/our_sincere_apologies_to_everyone_corepack/
- **Ocean of Games** : Malware riski ve daha önce miner yaptıkları tespit edilmiş. Kaynak: https://old.reddit.com/r/Piracy/comments/8yjv8e/do_not_use_ocean_of_games/
- **NosTEAM** : Sıkıntılı reklamlar ve 3mb boyutundaki şüpheli torrent dosyaları yüzünden güvenilir bir site değil.
- **igg-games.com / pcgamestorrents.com / gamestorrent.co** : Birçok forumda ve subredditte yasaklanmış bir site, adminlerin boktan tavırları ve oyunlara kendi DRM dosyalarını enjekte etmelerinden dolayı güvenmediğimiz bir site. Kaynak: https://www.reddit.com/r/Piracy/comments/c7356i/igg_infecting_their_downloads_with_their_selfmade/ / https://www.reddit.com/r/Piracy/comments/gv6zmu/why_igg_games_seems_hated/ / https://www.reddit.com/r/Piracy/comments/apcvx4/here_we_go_again_igg_this_time_im_not_reporting/ / https://old.reddit.com/r/Piracy/comments/afctdw/once_again_with_the_pirated_release_of_the/ 
- Adında herhangi bir scene grubu geçen bir siteyi kullanmayın (CODEX, CPY, SKIDROW, HOODLUM, RELOADED, SiMPLEX, DARKSiDERS, PLAZA gibi) scene grupları kendi adlarında bir domaindeki siteleri paylaşım yapmak için kullanmazlar. Bu tarz siteler size malware bulaştırmak amacıyla açılıyor.
- Google'da "GTA V ücretsiz indir" "GTA V crack indir" ya da "GTA V download crack" gibi basit aramalarla bulduğunuz sonuçları kullanmayın, bunların %99'u zararlıdır.
- Youtube üzerinde bulduğunuz herhangi bir crack oyun indirme rehberi veya linkini kullanmayın.



